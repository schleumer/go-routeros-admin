export class User
  (obj) ->
    if obj
      {
        this.name,
        this.email,
        this.username,
        this.powers
      } = obj

  name: "Guest"
  email: "guest@example.com"
  username: "guest"
  powers: []
  guest: true

  has: (t) ->
    if @powers
      @powers.filter((x) -> x.name == t).length > 0
    else
      false