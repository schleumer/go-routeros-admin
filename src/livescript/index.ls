# god bless the gambiarra
$ = window.jQuery = window.$ = require('jquery')

require('bootstrap')
require('angular')
require('angular-route')
#require('./modules/partials.ls')
require('nanoscroller')

#require('nested')

#jquery-bridget = require('jquery-bridget')
#packery = require('packery')
#$.bridget('packery', packery)
#
#jquery-bridget = require('jquery-bridget')
#masonry = require('masonry-layout')
#$.bridget('masonry', masonry)
#
#jquery-bridget = require('jquery-bridget')
#isotope = require('isotope-layout')
#$.bridget('isotope', isotope)

window.counter = 1

require! './models/user.ls': {
  User
}

app = angular.module('Mercury', ['ngRoute'/*, 'partials'*/])
routePrefix = '!'

app.config((require './config.ls')(routePrefix))

app.run ['$rootScope', '$timeout', '$location',
  ($rootScope, $timeout, $location) !->
    $rootScope.loading = true
]

app.service     '$moment',      require './providers/moment.ls'
app.service     '$navigate',    require './providers/navigate.ls'
app.service     '$storage',    require './providers/storage.ls'

app.directive   'eatClick',     require './directives/eat-click.ls'
app.directive   'nanoScroller', require './directives/nano-scroller.ls'

app.controller  'IndexCtrl',    require './controllers/index.ls'

app.filter      'i18n',         require './filters/i18n.ls'