_ = require 'prelude-ls'

module.exports = ['$data', '$timeout', '$navigate', ($data, $timeout, $navigate) ->
  {
    templateUrl: '/templates/directives/blocks.html'
    link: (scope, elem, attrs) ->
      scope.blocks = []
      blocks-container = $(elem).find('.blocks')
      # snakeCase because of html
      scope.getBlockClasses = (i) -> 
        (switch i.category
          case "ad-1" 
            ["block-w2", "block-h2"]
          case "ad-2"
            ["block-w1", "block-h2"]
          case "ad-3" 
            ["block-w2", "block-h1"]
          default
            ["block-w1", "block-h1"])
        .concat (if i.image then ['with-image'] else [])

      scope.getBlockStyle = (block) ->
        obj = {}
        if block.image
          obj["background-image"] = "url(#{block.image})"
        obj

      scope.getIconClass = (block) ->
        return ["fa", block.icon] if block.icon

      scope.navigate = (link) !-> $navigate.go(link.url, link.internal)
      scope.getUrl = (link) -> $navigate.build(link.url, link.internal)

      isoOptions = {
        animationEngine: 'css',
        layoutMode: 'masonry',
        itemSelector: '.block',
        masonry: {
          columnWidth: '.grid-sizer',
          itemSelector: '.block',
          gutter: '.gutter-sizer'
        }/*,
        getSortData: {
          weight: (itemElem) ->
            return parseInt(itemElem.getAttribute('priority'));
        },
        sortBy: 'weight'*/
      }

      /**/
      #$(elem).find('.blocks').packery({
      #  columnWidth: '.grid-sizer',
      #  itemSelector: '.block',
      #  gutter: '.gutter-sizer',
      #  rowHeight: 100
      #})
      #$(elem).find('.blocks').masonry({
      #  columnWidth: '.grid-sizer',
      #  itemSelector: '.block',
      #  gutter: '.gutter-sizer'
      #})

      scope.$watch 'blocks', ->
        return if scope.blocks.length < 1
        $timeout (->
          blocks-container.isotope(isoOptions)
          return
        )
        return

      $data.on 'blocks', (v) ->
        return if not v
        # QUE GAMBIARRA!!!!!!!!!!!
        scope.blocks = v |> _.filter (-> (/^ad-/.test it.category))
        return
  }
]