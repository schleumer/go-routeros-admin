module.exports = ['$rootScope', '$api', '$location',
($rootScope, $api, $location) ->
  @build = (url, internal) ->
    return url if not internal 
    $rootScope.buildUrl(url)

  @go = (url, internal) ->
    (window.open url, '_blank') if not internal
    ($location.path url) if internal
    return
  return
]