walk-sync = (dir, filelist, root) ->
  dir = dir.concat '/' if not (dir[dir.length - 1] is '/')
  if not root
    root = dir
  fs = fs or require 'fs'
  files = fs.readdirSync dir
  filelist = filelist or []
  files.forEach ((file) ->
    if (fs.statSync dir + file).isDirectory! then filelist := walk-sync dir + file + '/', filelist, root else filelist.push (dir + file).replace(root, ''))
  filelist

module.exports = walk-sync