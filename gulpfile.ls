#process.env["DISABLE_NOTIFIER"] = true


gulp = require 'gulp'
coffee = require 'gulp-coffee'
browserify = require 'browserify'
watchify = require 'watchify'
gutil = require 'gulp-util'
sourcemaps = require 'gulp-sourcemaps'
transform = require 'vinyl-transform'
uglify = require 'gulp-uglify'
connect = require 'gulp-connect'
jade = require 'gulp-jade'
watch = require 'gulp-watch'
del = require 'del'
concat = require 'gulp-concat'
less = require 'gulp-less'
path = require 'path'
gulpLiveScript = require 'gulp-livescript'
merge = require 'merge-stream'
streamqueue = require 'streamqueue'
source = require 'vinyl-source-stream'
fs = require 'fs'
plumber = require 'gulp-plumber'
notify = require 'gulp-notify'
insert = require 'gulp-insert'

gulp.task 'clean:scripts', (cb) -> del ['public/js'], cb

gulp.task 'clean:stylesheet', (cb) -> del ['public/css'], cb

gulp.task 'clean:templates', (cb) -> del ['public/templates', 'public/index.html', 'public/layouts'], cb

gulp.task 'clean:files', (cb) -> del ['public/fonts', 'public/images', 'public/api'], cb

gulp.task 'copy-fontawesome', ->
  gulp.src('./public/components/font-awesome/fonts/**/*', 
    {base: './public/components/font-awesome/'})
    .pipe(gulp.dest('./public/'))

#gulp.task 'copy-api', ->
#  gulp.src('./src/api/**/*', {base: './src/'})
#    .pipe(watch('./src/api/**/*', {base: './src/'}))
#    .pipe(gulp.dest('./public/'))

gulp.task 'copy-fonts', ->
  gulp.src('./src/fonts/**/*', {base: './src/'})
    .pipe(watch('./src/fonts/**/*', {base: './src/'}))
    .pipe(gulp.dest('./public/'))

gulp.task 'copy-images', ->
  gulp.src('./src/images/**/*', {base: './src/'})
    .pipe(watch('./src/images/**/*', {base: './src/'}))
    .pipe(gulp.dest('./public/'))

gulp.task 'ls', ->
  b = watchify(browserify("./src/livescript/index.ls", watchify.args))
  b.transform('liveify')
  b.bundle()
    .on('error', notify.onError("Error compiling livescript!\n <%= error.message %>"))
    .pipe(source('app.js'))
    .pipe(gulp.dest('./public/js'))
    .pipe(notify(message: "Livescript compiled!", on-last: true))

gulp.task 'prepend', ['ls'], ->
  gulp.src('./public/js/app.js')
    .pipe(insert.prepend(fs.readFileSync('./src/helpers/app.js.banner')))
    .pipe(gulp.dest('./public/js'))

gulp.task 'templates', ->
  locals = require './src/helpers/jade-locals'

  jadeTask = jade {locals: locals, pretty: true}

  jadeTask.on('error', notify.onError("Error compiling jade!\n <%= error.message %>"))

  gulp.src('./src/jade/**/*.jade')
    .pipe(jadeTask)
    .pipe(gulp.dest('./public/'))
    .pipe(notify(message: "Jade compiled!", on-last: true))

gulp.task 'stylesheet', ->
  gulp.src(['./src/less/main.less', './src/less/themes/*.less'], {base: './src/less/'})
    .pipe(plumber({errorHandler: notify.onError("Error compiling LESS\n <%= error.message %>")}))
    .pipe(less({
      paths: [path.join __dirname, 'public', 'components'],
      relativeUrls: false,
      globalVars: {
        defaultTheme: 'paper'
      }
    }))
    .pipe(gulp.dest('./public/css'))
    .pipe(notify(message: "LESS compiled!", on-last: true))

gulp.task 'connect', [
  'templates'
], ->
  connect.server {
    root: 'public'
    port: 8081
    livereload: true
  }

# Watch stuffs

gulp.task 'ls-watch', ->
  watch('src/livescript/**/*.ls', ['ls'], -> 
    gulp.start('ls'))

gulp.task 'stylesheet-watch', ['stylesheet'], ->
  watch('src/less/**/*.less', -> 
    gulp.start('stylesheet'))

gulp.task 'templates-watch', ['templates'], ->
  watch('src/jade/**/*.jade', -> 
    gulp.start('templates'))

gulp.task 'clean', [
  'clean:scripts',
  'clean:stylesheet',
  'clean:templates',
  'clean:files'
]

gulp.task 'default', [
  'ls'
  'prepend'
  'stylesheet'
  'templates'
  'ls-watch'
  'stylesheet-watch'
  'templates-watch'
  'copy-fontawesome'
  #'copy-api'
  'copy-fonts'
  'copy-images'
  #'connect'
]

gulp.task 'only-build', [
  'ls'
  'prepend'
  'stylesheet'
  'templates'
  'ls-watch'
  'stylesheet-watch'
  'templates-watch'
  'copy-fontawesome'
  'copy-api'
  'copy-fonts'
  'copy-images'
]