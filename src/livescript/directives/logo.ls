module.exports = ['$data', ($data) ->
  {
    templateUrl: '/templates/directives/logo.html'
    link: (scope, elem, attrs) ->
      $data.on 'logo', (v) ->
        scope.logo  = v
  }
]