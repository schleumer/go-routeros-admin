package main

import (
    "fmt"
    "github.com/julienschmidt/httprouter"
    "github.com/schleumer/routeros-api-go"
    "net/http"
    "encoding/json"
    "log"
    "os"
    "bytes"
    "regexp"
)

type IndexResponse struct {
  Lease []map[string]string
  Mangle []map[string]string
  AddressList []map[string]string
  CurrentMangle map[string]string
  CurrentLease map[string]string
  CurrentAddress map[string]string
}

func Map(f func(A) B, xs []A) []B {
    ys := make([]B, len(xs))
    for i, x := range xs {
        ys[i] = f(x)
    }
    return ys
}

func GetConnection()*routeros.Client {
  connection, err := routeros.New("10.1.1.1:8728")

  if err != nil {
    log.Fatal(err)
  }

  err = connection.Connect("admin", "wes_130290")
  if err != nil {
    println("Erro ao autorizar")
  }

  return connection
}

func ShitHappens(err error) {
  if err != nil {
    log.Fatal(err)
  }
}

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
  connection := GetConnection()

  re := regexp.MustCompile("\\:([^:]*?)$")

  var addresses = r.Header["X-Real-Ip"]
  var firstAddr string = ""

  if len(addresses) > 0 {
    firstAddr = addresses[0]
  } else {
    firstAddr = r.RemoteAddr
  }

  fmt.Printf("%s\n", firstAddr)

  var remoteAddr string = re.ReplaceAllString(firstAddr, "")

  if remoteAddr == "127.0.0.1" {
    remoteAddr = "10.2.1.13"
  }

  lease, err := connection.Call("/ip/dhcp-server/lease/print", nil)

  ShitHappens(err)

  mangle, err := connection.Call("/ip/firewall/mangle/print", nil)

  ShitHappens(err)

  var q []routeros.Pair

  q = append(q, routeros.Pair{Key: "where", Value: "list=\"3G users\"", Op: "="})

  address_list, err := connection.Call("/ip/firewall/address-list/print", q)

  ShitHappens(err)

  var currentLease map[string]string = nil
  var currentMangle map[string]string = nil
  var currentAddress map[string]string = nil

  for _, v := range mangle.SubPairs {
    if v["src-address"] == remoteAddr {
      currentMangle = v
    }
  }

  for _, v := range lease.SubPairs {
    if v["address"] == remoteAddr {
      currentLease = v
    }
  }

  for _, v := range address_list.SubPairs {
    println(v["address"], remoteAddr)
    if v["address"] == remoteAddr {
      currentAddress = v
    }
  }


  res := IndexResponse{ lease.SubPairs, mangle.SubPairs, address_list.SubPairs, currentMangle, currentLease, currentAddress }

  js, _ := json.Marshal(res)
  var jsOut bytes.Buffer
  json.Indent(&jsOut, js, "", "\t")
  out := string(jsOut.Bytes())
  fmt.Fprint(w, out)
}

func EnableMangle(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
  connection := GetConnection()

  //io.ReadAll()
  buf := new(bytes.Buffer)
  buf.ReadFrom(r.Body)

  type EnableBody struct {
    Id string `json:"id"`
    Disabled string `json:"disabled"`
  }

  var req EnableBody

  json.Unmarshal(buf.Bytes(), &req)

  var q []routeros.Pair

  q = append(q, routeros.Pair{Key: "numbers", Value: req.Id, Op: "="})

  if req.Disabled == "false" {
    _, err := connection.Call("/ip/firewall/mangle/enable", q)
    ShitHappens(err)
  } else {
    _, err := connection.Call("/ip/firewall/mangle/disable", q)
    ShitHappens(err)
  }
  

  fmt.Fprint(w, "ok")
}

func main() {
  router := httprouter.New()

  router.GET("/api", Index)
  router.POST("/api/set-mangle", EnableMangle)

  router.NotFound = http.FileServer(http.Dir("../public")).ServeHTTP

  log.Fatal(http.ListenAndServe(":" + os.Getenv("PORT"), router))
}