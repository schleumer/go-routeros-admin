module.exports = ['$rootScope', '$api', ($rootScope, $api) ->
  data = {}
  @on = (k, cb) ->
    (cb data[k])
    $rootScope.$on '$data.changed', ->
      (cb data[k])
  @get = (k) -> data[k]

  $api.get('data').success (res) ->
    data := res.result
    $rootScope.$broadcast '$data.changed'
    return
  return
]