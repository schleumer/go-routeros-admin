angular = require 'angular'
_ =       require 'prelude-ls'

module = angular.module('partials', [])

module.run ['$templateCache', ($templateCache) !->
  templates = window.__templateStorage
  (_.each (!-> ($templateCache.put (_.first it), (_.last it))), templates)
]

module.service '$templateStorage', ['$templateCache', ($templateCache) ->
  @get = (name) ->
    $templateCache.get(name)
  return
]


